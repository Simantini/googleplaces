Google Places POC

Workflow:
1. Search Screen : User enters the place name to search and search results are poplulated in the list below.
On click of any list item from the search results, Search Details screen is displayed 

2. Search Details Screen : It displays a map of the place entered by the user on previous screen. 
It also displays the photos related to the place .On click of any photo, that photo is downloaded and saved in device's sd card.

Following libraries used:
Retrofit: used for network communication
Glide: used to download and display images
gson: Parse json response into POJOs

Apart from these libraries, Google Places Api Webservices are used to fetch search results, to display data on map along with their photos.
