package com.simantini.googleplaces;

import com.simantini.googleplaces.data.model.response.SearchResponse;
import com.simantini.googleplaces.searchplaces.SearchPlacesContract;
import com.simantini.googleplaces.searchplaces.SearchPlacesPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


/**
 * Created by simantini.ranade on 24-06-2017. hello
 */
@RunWith(MockitoJUnitRunner.class)
public class SearchPlacesPresenterTest {

    private SearchPlacesPresenter presenter;

    @Mock
    private SearchPlacesContract.SearchPlacesView view;

    @Before
    public void setup() {
        presenter = new SearchPlacesPresenter(view);
    }

    @Test
    public void checkIfShowsMessageOnItemClick() {
        presenter.onItemClicked(1);
        verify(view, times(1)).handleItemClick(1);
    }

    @Test
    public void autoSearchTest() {
        presenter.autoSearch("aj");
        verify(view, times(1)).postSearchResults((SearchResponse) anyObject());
        //verify(view, times(1)).showMessage(anyString());
    }
}
