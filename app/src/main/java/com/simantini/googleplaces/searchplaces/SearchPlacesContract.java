package com.simantini.googleplaces.searchplaces;


import android.app.Activity;

import com.simantini.googleplaces.data.model.response.SearchResponse;

/**
 * Created by simantini.ranade on 24-06-2017.
 */
public class SearchPlacesContract {

    /**
     * implemented by Activity
     */
    public interface SearchPlacesView {
        void postSearchResults(SearchResponse searchResponse);

        void handleItemClick(int position);

        void showErrorMessage(String s);

        Activity getContext();
    }

    /*
    implemented by presenter
     */
    public interface Presenter {
        void onCreate();

        void onPause();

        void onResume();

        void onDestroy();

        void onItemClicked(int position);

        void autoSearch(String input);
    }
}
