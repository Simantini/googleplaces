package com.simantini.googleplaces.searchplaces;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Toast;

import com.simantini.googleplaces.R;
import com.simantini.googleplaces.constants.AppConstants;
import com.simantini.googleplaces.data.model.response.SearchResponse;
import com.simantini.googleplaces.placesdetails.PlaceDetailsActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Activity class to display the UI for search screen
 * <p>
 * Created by simantini.ranade on 24-06-2017.
 */
public class SearchPlacesActivity extends AppCompatActivity implements SearchPlacesContract.SearchPlacesView, AdapterView.OnItemClickListener {
    private static String TAG = SearchPlacesActivity.class.getName();
    private SearchPlacesPresenter presenter = new SearchPlacesPresenter(this);
    private ArrayList resultList = new ArrayList();
    private GooglePlacesAutocompleteAdapter googlePlacesAdapter;
    private SearchResponse response;

    private Timer timer = new Timer();
    final int DELAY = 1000; //milliseconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initUi();
        presenter.onCreate();

    }


    /**
     * method called to initiate the UI
     */
    private void initUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);

        googlePlacesAdapter = new GooglePlacesAutocompleteAdapter(this, R.layout.layout_search_item);
        autoCompView.setAdapter(googlePlacesAdapter);
        autoCompView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        autoCompView.setText("");
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    /**
     * method called with search results to be shown below the autocomplete search box
     *
     * @param searchResponse
     */
    @Override
    public void postSearchResults(SearchResponse searchResponse) {
        response = searchResponse;
        try {
            if (searchResponse != null &&
                    searchResponse.getPredictions() != null && searchResponse.getPredictions().size() > 0) {
                List<SearchResponse.Predictions> predictions = searchResponse.getPredictions();
                if (predictions != null && predictions.size() > 0 && resultList != null) {
                    resultList.clear();
                }
                for (int i = 0; i < predictions.size(); i++) {
                    SearchResponse.Predictions prediction = predictions.get(i);

                    resultList.add(predictions.get(i).getDescription());
                }
            }
            // Assign the data to the FilterResults
            if (resultList != null && resultList.size() > 0) {
                googlePlacesAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            Log.e(TAG, "Cannot process JSON results", e);
        }
    }

    /**
     * method to launch search results screen for a specific place
     *
     * @param position
     */
    @Override
    public void handleItemClick(int position) {
        if (response != null &&
                response.getPredictions() != null && response.getPredictions().size() > 0) {
            List<SearchResponse.Predictions> predictions = response.getPredictions();
            //Toast.makeText(this, place, Toast.LENGTH_SHORT).show();
            SearchResponse.Predictions prediction = predictions.get(position);
            Intent intent = new Intent(this, PlaceDetailsActivity.class);

            intent.putExtra(AppConstants.INTENT_PLACE_ID, prediction.getPlace_id());

            startActivity(intent);

        }


    }

    /**
     * method to display error message
     *
     * @param s
     */
    @Override
    public void showErrorMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public Activity getContext() {
        return this;
    }

    /**
     * method called when the list item from the autocomplete search results list is clicked
     *
     * @param adapterView
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView adapterView, View view, int position, long id) {
        String str = (String) adapterView.getItemAtPosition(position);
        presenter.onItemClicked(position);

    }

    /**
     * adapter to inflate the autocomplete list to display the search results
     */
    class GooglePlacesAutocompleteAdapter extends ArrayAdapter implements Filterable {


        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            if (resultList != null) {
                return resultList.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int index) {
            if (resultList != null) {
                return resultList.get(index);
            }
            return "";

        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        final String inputText = constraint.toString();
                        /**
                         * Search api is hit after every character entered in the search box. so to reduce the number of api hits
                         * timer is used which will query the search after 1 second.
                         * This will optimize the number of API hits.
                         */
                        timer.cancel();
                        timer = new Timer();
                        timer.schedule(
                                new TimerTask() {
                                    @Override
                                    public void run() {
                                        presenter.autoSearch(inputText);

                                    }
                                },
                                DELAY
                        );
                        // Retrieve the autocomplete results.
//                        resultList = autocomplete(constraint.toString());
//
//                        // Assign the data to the FilterResults
                        if (resultList != null) {
                            filterResults.values = resultList;
                            filterResults.count = resultList.size();
                        }
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}
