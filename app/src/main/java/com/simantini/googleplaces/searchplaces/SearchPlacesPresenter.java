package com.simantini.googleplaces.searchplaces;

import android.util.Log;

import com.simantini.googleplaces.R;
import com.simantini.googleplaces.constants.APIConstants;
import com.simantini.googleplaces.data.model.response.SearchResponse;
import com.simantini.googleplaces.data.source.remote.APISearchHelper;


/**
 * Presenter class to manage the business logic
 * Created by simantini.ranade on 24-06-2017.
 */
public class SearchPlacesPresenter implements SearchPlacesContract.Presenter {

    private static String TAG = SearchPlacesPresenter.class.getName();
    private SearchPlacesContract.SearchPlacesView view;

    public SearchPlacesPresenter(SearchPlacesContract.SearchPlacesView view) {
        this.view = view;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onItemClicked(int position) {
        view.handleItemClick(position);
    }

    /**
     * method called to fetch search results for a given search query
     *
     * @param input
     */
    @Override
    public void autoSearch(String input) {

        APISearchHelper apiSearchHelper = new APISearchHelper() {
            @Override
            protected void onGetSearchResponseSuccess(SearchResponse searchResponse) {


                if (searchResponse.getStatus().equalsIgnoreCase(APIConstants.OVER_QUERY_LIMIT)) {
                    view.showErrorMessage("You have exceeded your daily request quota for this API.");
                } else {
                    if (searchResponse != null) {
                        try {
                            view.postSearchResults(searchResponse);
                        } catch (Exception e) {
                            Log.e(TAG, "Cannot process JSON results", e);
                            view.showErrorMessage("Error in search results");
                        }
                    } else {
                        view.showErrorMessage("Error in search resuls");
                    }
                }

            }

            @Override
            protected void onGetSearchResponseFailure(Throwable t) {
                view.showErrorMessage("Error in search results");
            }
        };

        apiSearchHelper.search(input,view.getContext().getString(R.string.api_key));
    }
}
