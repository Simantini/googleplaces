package com.simantini.googleplaces.placesdetails;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.simantini.googleplaces.R;

import java.util.ArrayList;


/**
 * adapter class to inflate the photos list
 * Created by simantini.ranade on 25-06-2017.
 */

public class PhotoViewAdapter extends RecyclerView.Adapter<PhotoViewAdapter.PhotoListHolder> {

    private PlaceDetailsActivity context;
    private PlaceDetailsPresenter presenter;
    private ArrayList<String> photoList = new ArrayList<>();

    public PhotoViewAdapter(PlaceDetailsActivity context, PlaceDetailsPresenter presenter) {
        this.context = context;
        this.presenter = presenter;

    }

    /**
     * initialize the photo list
     *
     * @param photoList
     */
    public void setPhotoList(ArrayList<String> photoList) {
        this.photoList.clear();
        this.photoList.addAll(photoList);
    }

    @Override
    public PhotoListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_list_item, parent,
                false);
        return new PhotoListHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoListHolder holder, final int position) {
        Glide.with(context).load(photoList.get(holder.getAdapterPosition())).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.handlePhotoClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photoList.size();
    }

    public static class PhotoListHolder extends RecyclerView.ViewHolder {
        final ImageView imageView;

        public PhotoListHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.ivPhoto);
        }

    }
}
