package com.simantini.googleplaces.placesdetails;

import android.app.Activity;

import com.simantini.googleplaces.data.model.response.PlaceDetailsResponse;

import java.util.ArrayList;

/**
 * Created by simantini.ranade on 25-06-2017.
 */
public interface PlaceDetailsContract {

    /**
     * implemented by {@link PlaceDetailsActivity}
     */
    interface PlaceDetailsView {
        void postPlaceDetails(PlaceDetailsResponse placeDetailsResponse);

        void updateList(ArrayList<String> url);

        void photoDownloadSuccess();

        Activity getContext();

        boolean checkPermission();

        void showProgressDialog();

        void dismissProgressDialog();

        void showErrorMessage(String s);
    }

    /**
     * implemented by {@link PlaceDetailsPresenter}
     */
    interface Presenter {

        void onCreate();

        void onPause();

        void onResume();

        void onDestroy();

        void resumePhotoDownload();

        void getPlaceDetails(String placeId);

        void handlePhotoClick(int position);
    }
}
