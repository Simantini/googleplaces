package com.simantini.googleplaces.placesdetails;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.simantini.googleplaces.R;
import com.simantini.googleplaces.constants.AppConstants;
import com.simantini.googleplaces.data.model.response.PlaceDetailsResponse;

import java.util.ArrayList;

/**
 * Activity class to display place details on a map and photos for the corresponding place
 * <p>
 * Created by simantini.ranade on 25-06-2017.
 */
public class PlaceDetailsActivity extends AppCompatActivity implements OnMapReadyCallback, PlaceDetailsContract.PlaceDetailsView {

    private PlaceDetailsPresenter presenter = new PlaceDetailsPresenter(this);
    private PlaceDetailsResponse placeDetailsResponse;
    private GoogleMap map;
    private static final int WRITE_PERMISSION = 101;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        presenter.getPlaceDetails(getIntent().getStringExtra(AppConstants.INTENT_PLACE_ID));
        presenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }


    /**
     * method to initialize ui
     */
    private void init() {
        FragmentManager myFragmentManager = getSupportFragmentManager();
        SupportMapFragment mapFragment = (SupportMapFragment) myFragmentManager
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        gridLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);

        RecyclerView rvPhotos = (RecyclerView) findViewById(R.id.rvPhotos);
        rvPhotos.setLayoutManager(gridLayoutManager);
        rvPhotos.setAdapter(presenter.getAdapter());

        //mPresenter.setViewAndData(this, mLatLng);
        // mFragmentPlaceDetailsBinding.rvPhotos.setAdapter(mPresenter.getAdaptor());

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Double latitude = 0.0;
        Double longitude = 0.0;
        if (placeDetailsResponse != null) {
            latitude = Double.valueOf(placeDetailsResponse.getResult().getGeometry().getLocation().getLat());
            longitude = Double.valueOf(placeDetailsResponse.getResult().getGeometry().getLocation().getLng());
        }
        // Add a marker in Sydney, Australia, and move the camera.
        LatLng place = new LatLng(latitude, longitude);
        map.addMarker(new MarkerOptions().position(place).title(placeDetailsResponse.getResult().getName()));
        map.moveCamera(CameraUpdateFactory.newLatLng(place));
    }

    /**
     * method called when place details response is received
     *
     * @param placeDetailsResponse
     */
    @Override
    public void postPlaceDetails(PlaceDetailsResponse placeDetailsResponse) {
        this.placeDetailsResponse = placeDetailsResponse;
        init();
    }

    /**
     * method called to update the photos list
     *
     * @param urls
     */
    @Override
    public void updateList(ArrayList<String> urls) {
        presenter.getAdapter().setPhotoList(urls);
        presenter.getAdapter().notifyDataSetChanged();
    }

    /**
     * method to display photo download success
     */
    @Override
    public void photoDownloadSuccess() {
        Toast.makeText(this, getString(R.string.msg_photo_download_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public Activity getContext() {
        return this;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case WRITE_PERMISSION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    presenter.resumePhotoDownload();

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public boolean checkPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_PERMISSION);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void showProgressDialog() {
        // Create a progressdialog
        progressDialog = new ProgressDialog(this);
        // Set progressdialog title
        progressDialog.setTitle("Download Photo");
        // Set progressdialog message
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(false);
        // Show progressdialog
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        // Close progressdialog
        progressDialog.dismiss();
    }

    /**
     * display error message
     *
     * @param s
     */
    @Override
    public void showErrorMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
