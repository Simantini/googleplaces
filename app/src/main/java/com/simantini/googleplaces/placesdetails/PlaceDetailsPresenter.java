package com.simantini.googleplaces.placesdetails;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.simantini.googleplaces.R;
import com.simantini.googleplaces.constants.APIConstants;
import com.simantini.googleplaces.data.model.response.NearbyPlacesResponse;
import com.simantini.googleplaces.data.model.response.PlaceDetailsResponse;
import com.simantini.googleplaces.data.source.remote.APINearbyPlacesHelper;
import com.simantini.googleplaces.data.source.remote.APIPlaceDetailsHelper;
import com.simantini.googleplaces.data.source.remote.PhotosDownloadManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Presenter class which consists of business logic to fetch photos list, update ui accordingly and handle photos item click
 * <p>
 * Created by simantini.ranade on 25-06-2017.
 */
public class PlaceDetailsPresenter implements PlaceDetailsContract.Presenter {
    private static String TAG = PlaceDetailsPresenter.class.getName();
    private PlaceDetailsContract.PlaceDetailsView view;
    private PhotoViewAdapter adapter;
    private ArrayList<String> photoUrls;
    private String savedPhotoUrl;
    private PhotosDownloader photosDownloader;

    public PlaceDetailsPresenter(PlaceDetailsContract.PlaceDetailsView view) {

        this.view = view;

        photoUrls = new ArrayList<>();
        adapter = new PhotoViewAdapter((PlaceDetailsActivity) view, this);
    }

    public PhotoViewAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onPause() {
        if (photosDownloader != null && photosDownloader.getStatus() == AsyncTask.Status.RUNNING) {
            photosDownloader.cancel(true);
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onDestroy() {
        if (photosDownloader != null && photosDownloader.getStatus() == AsyncTask.Status.RUNNING) {
            photosDownloader.cancel(true);
        }
    }


    /**
     * resume download after permission for writing sd card is granted by user
     */
    @Override
    public void resumePhotoDownload() {
        photosDownloader = new PhotosDownloader();
        photosDownloader.execute(savedPhotoUrl);
    }

    private void addItemToPhotoList(final String photoReference) {
        String url = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + photoReference +
                "&key=" + view.getContext().getString(R.string.api_key);
        photoUrls.add(url);
        //Log.i(TAG,"addItemToPhotoList url: "+url);

    }

    private void updateView(ArrayList<String> photoUrls) {
        Log.i(TAG, "updateView count of urls " + photoUrls.size());
        view.updateList(this.photoUrls);
    }

    /**
     * method to fetch the place details for a given place
     *
     * @param placeId
     */
    @Override
    public void getPlaceDetails(String placeId) {
        APIPlaceDetailsHelper apiPlaceDetailsHelper = new APIPlaceDetailsHelper() {
            @Override
            protected void onGetPlaceDetailsResponseSuccess(PlaceDetailsResponse placeDetailsResponse) {
                if (placeDetailsResponse.getStatus().equalsIgnoreCase(APIConstants.OVER_QUERY_LIMIT)) {
                    view.showErrorMessage("You have exceeded your daily request quota for this API.");
                } else {
                    if (placeDetailsResponse != null) {
                        try {
                            view.postPlaceDetails(placeDetailsResponse);
                            /**
                             * call near by webservices to fetch photos.
                             */
                            fetchPhotos(placeDetailsResponse);

                        } catch (Exception e) {
                            Log.e(TAG, "Cannot process JSON results", e);
                            view.showErrorMessage("Error in fetching places");
                        }
                    } else {
                        view.showErrorMessage("Error in fetching places");
                    }
                }
            }

            @Override
            protected void onGetPlaceDetailsResponseFailure(Throwable t) {
                view.showErrorMessage("Error in fetching places");
            }
        };

        apiPlaceDetailsHelper.getPlaceDetails(placeId,view.getContext().getString(R.string.api_key));
    }

    /**
     * The place details api does not give valid photo results sometimes, so we need to call
     * nearby places api which takes location returned form place details api as input
     * and returns valid photo results
     *
     * @param placeDetailsResponse
     */
    private void fetchPhotos(PlaceDetailsResponse placeDetailsResponse) {
        APINearbyPlacesHelper apiNearbyPlacesHelper = new APINearbyPlacesHelper() {
            @Override
            protected void onGetNearbyPlacesResponseSuccess(NearbyPlacesResponse nearbyPlacesResponse) {
                if (nearbyPlacesResponse.getStatus().equalsIgnoreCase(APIConstants.OVER_QUERY_LIMIT)) {
                    view.showErrorMessage("You have exceeded your daily request quota for this API.");
                } else {
                    if (nearbyPlacesResponse != null) {
                        try {
                            ArrayList<NearbyPlacesResponse.Result> resultList = (ArrayList<NearbyPlacesResponse.Result>) nearbyPlacesResponse.getResults();
                            if (resultList != null) {
                                photoUrls.clear();
                                for (NearbyPlacesResponse.Result result : resultList) {


                                    List<NearbyPlacesResponse.Photo> photos = result.getPhotos();
                                    if (photos != null) {
                                        for (NearbyPlacesResponse.Photo photo : photos) {
                                            String photoReference = photo.getPhotoReference();
                                            if (!TextUtils.isEmpty(photoReference)) {
                                                addItemToPhotoList(photoReference);
                                            }
                                        }
                                        updateView(photoUrls);
                                    }

                                }
                            }

                        } catch (Exception e) {
                            Log.e(TAG, "Cannot process JSON results", e);
                            view.showErrorMessage("Error in fetching places");
                        }
                    } else {
                        view.showErrorMessage("Error in fetching places");
                    }
                }
            }

            @Override
            protected void onGetNearbyPlacesResponseFailure(Throwable t) {
                view.showErrorMessage("Error in fetching places");
            }
        };


        String location = placeDetailsResponse.getResult().getGeometry().getLocation().getLat() + "," +
                placeDetailsResponse.getResult().getGeometry().getLocation().getLng();
        apiNearbyPlacesHelper.getNearbyPlaces(location, 500,view.getContext().getString(R.string.api_key));
    }


    /**
     * method to handle the photo click
     *
     * @param position
     */
    @Override
    public void handlePhotoClick(int position) {
        if (PhotosDownloadManager.isSdCardPresent()) {
            try {
                if (photoUrls != null) {
                    savedPhotoUrl = photoUrls.get(position);
                    if (view.checkPermission()) {

                        // saveImageToFile(photoUrls.get(position));
                        photosDownloader = new PhotosDownloader();
                        photosDownloader.execute(photoUrls.get(position));
                    }
                }
            } catch (Exception exception) {
                view.showErrorMessage("Error in downloading photo");
            }
        } else {
            view.showErrorMessage("Sd Card not present");
        }
    }

    /**
     * download photos when user clicks on photo item and save to sd card
     */
    public class PhotosDownloader extends AsyncTask<String, Void, Bitmap> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showProgressDialog();
        }

        @Override
        protected Bitmap doInBackground(String... url) {
            Bitmap bitmap = null;
            try {
                bitmap = Glide.
                        with(view.getContext()).
                        load(url[0]).
                        asBitmap().
                        into(-1, -1).
                        get();
                return PhotosDownloadManager.savePhotoToDevice(bitmap);
            } catch (Exception e) {
                e.printStackTrace();
                return bitmap;
            }

        }

        @Override
        protected void onPostExecute(Bitmap result) {

            // Close progressdialog
            view.dismissProgressDialog();
            view.photoDownloadSuccess();
        }
    }

}
