package com.simantini.googleplaces.constants;

/**
 * Created by simantini.ranade on 24-06-2017.
 */
public class APIConstants {
    // TODO put production API
    public static final String REST_BASE_URI_PROD = "https://maps.googleapis.com/maps/api/";
    public static final String REST_BASE_URI_DEV = "https://maps.googleapis.com/maps/api/";

    public static final String REST_API_SEARCH = "place/autocomplete/json";
    public static final String REST_API_PLACE_DETAILS = "place/details/json";
    public static final String REST_API_NEARBY_PLACES = "place/nearbysearch/json";

    /**
     * Webservice status constants
     */
    public static final String OVER_QUERY_LIMIT = "OVER_QUERY_LIMIT";
    public static final String OK  = "OK";
    public static final String ZERO_RESULTS  = "ZERO_RESULTS";
    public static final String REQUEST_DENIED  = "REQUEST_DENIED";
    public static final String INVALID_REQUEST  = "INVALID_REQUEST";
}
