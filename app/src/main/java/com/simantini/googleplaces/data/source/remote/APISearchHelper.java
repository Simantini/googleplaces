package com.simantini.googleplaces.data.source.remote;

import android.util.Log;

import com.simantini.googleplaces.data.model.response.SearchResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * class to call Place Autocomplete API Google Places Api webservice
 * Created by simantini.ranade on 24-06-2017
 */
public class APISearchHelper extends AbstractAPIHelper<APISearch> {
    public APISearchHelper() {
        super(APISearch.class);
    }

    /**
     * method to fetch search results for a specific autocomplete input
     *
     *
     */
    public final void search(String input,String apikey) {

        Call<SearchResponse> call = mService.search(apikey,input);
        Log.i("APISearchHelper", "search " + call.request().url());
        call.enqueue(new Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {

                onGetSearchResponseSuccess(response.body());

                // Log.i("APITopicsHelper", "onResponse" + response.body().getMessage());
            }

            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                onGetSearchResponseFailure(t);
            }
        });
    }

    protected void onGetSearchResponseSuccess(SearchResponse resp) {
        throw new UnsupportedOperationException("Do not implement!");
    }

    protected void onGetSearchResponseFailure(Throwable t) {
        throw new UnsupportedOperationException("Do not implement!");
    }
}
