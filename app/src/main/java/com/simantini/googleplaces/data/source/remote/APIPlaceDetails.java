package com.simantini.googleplaces.data.source.remote;

import com.simantini.googleplaces.constants.APIConstants;
import com.simantini.googleplaces.data.model.response.PlaceDetailsResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * api to fetch search places details for a specific place
 * Created by simantini.ranade on 24-06-2017.
 */
public interface APIPlaceDetails {

    @POST(APIConstants.REST_API_PLACE_DETAILS)
    Call<PlaceDetailsResponse> getPlaceDetails(@Query("key") String API_KEY,
                                               @Query("placeid") String placeid);
}
