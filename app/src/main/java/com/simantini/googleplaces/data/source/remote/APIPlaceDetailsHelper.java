package com.simantini.googleplaces.data.source.remote;

import android.util.Log;

import com.simantini.googleplaces.data.model.response.PlaceDetailsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * class to call Place Details API Google Places Api webservice
 * Created by simantini.ranade on 24-06-2017
 */
public class APIPlaceDetailsHelper extends AbstractAPIHelper<APIPlaceDetails> {
    public APIPlaceDetailsHelper() {
        super(APIPlaceDetails.class);
    }

    /**
     * method to fetch search places details for a specific place
     *
     * @param placeid
     */
    public final void getPlaceDetails(String placeid,String apikey) {
        Log.i("APIPlaceDetailsHelper", "getPlaceDetails placeid: " + placeid);
        Call<PlaceDetailsResponse> call = mService.getPlaceDetails(apikey, placeid);
        Log.i("APIPlaceDetailsHelper", "getPlaceDetails url: " + call.request().url());
        call.enqueue(new Callback<PlaceDetailsResponse>() {
            @Override
            public void onResponse(Call<PlaceDetailsResponse> call, Response<PlaceDetailsResponse> response) {
                onGetPlaceDetailsResponseSuccess(response.body());
                // Log.i("APITopicsHelper", "onResponse" + response.body().getMessage());
            }

            @Override
            public void onFailure(Call<PlaceDetailsResponse> call, Throwable t) {
                onGetPlaceDetailsResponseFailure(t);
            }
        });
    }

    protected void onGetPlaceDetailsResponseSuccess(PlaceDetailsResponse resp) {
        throw new UnsupportedOperationException("Do not implement!");
    }

    protected void onGetPlaceDetailsResponseFailure(Throwable t) {
        throw new UnsupportedOperationException("Do not implement!");
    }
}
