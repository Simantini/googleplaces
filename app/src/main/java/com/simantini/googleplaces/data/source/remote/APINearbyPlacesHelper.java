package com.simantini.googleplaces.data.source.remote;

import android.util.Log;

import com.simantini.googleplaces.data.model.response.NearbyPlacesResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * class to call Near by search API Google Places Api webservice
 * Created by simantini.ranade on 26-06-2017
 */
public class APINearbyPlacesHelper extends AbstractAPIHelper<APINearbyPlaces> {
    public APINearbyPlacesHelper() {
        super(APINearbyPlaces.class);
    }

    /**
     * method to fetch nearby places which returns photo reference id
     *
     * @param latlong
     * @param radius
     */
    public final void getNearbyPlaces(String latlong, int radius,String apikey) {
        Call<NearbyPlacesResponse> call = mService.getNearbyPlaces(latlong, radius, apikey);
        Log.i("APINearbyPlacesHelper", "getNearbyPlaces url: " + call.request().url());
        call.enqueue(new Callback<NearbyPlacesResponse>() {
            @Override
            public void onResponse(Call<NearbyPlacesResponse> call, Response<NearbyPlacesResponse> response) {
                onGetNearbyPlacesResponseSuccess(response.body());
                // Log.i("APITopicsHelper", "onResponse" + response.body().getMessage());
            }

            @Override
            public void onFailure(Call<NearbyPlacesResponse> call, Throwable t) {
                onGetNearbyPlacesResponseFailure(t);
            }
        });
    }

    protected void onGetNearbyPlacesResponseSuccess(NearbyPlacesResponse resp) {
        throw new UnsupportedOperationException("Do not implement!");
    }

    protected void onGetNearbyPlacesResponseFailure(Throwable t) {
        throw new UnsupportedOperationException("Do not implement!");
    }
}
