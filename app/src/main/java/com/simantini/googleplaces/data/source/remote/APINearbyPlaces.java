package com.simantini.googleplaces.data.source.remote;

import com.simantini.googleplaces.constants.APIConstants;
import com.simantini.googleplaces.data.model.response.NearbyPlacesResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * class to fetch valid photo results from location given as input
 * simantini.ranade on 26-06-2017.
 */
public interface APINearbyPlaces {

    @POST(APIConstants.REST_API_NEARBY_PLACES)
    Call<NearbyPlacesResponse> getNearbyPlaces(@Query("location") String location,
                                               @Query("radius") int radius,
                                               @Query("key") String key);
}
