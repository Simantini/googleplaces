package com.simantini.googleplaces.data.source.remote;

/**
 * Created by simantini.ranade on 24-06-2017.
 */
abstract class AbstractAPIHelper<S> {
    protected S mService;

    public AbstractAPIHelper(Class<S> serviceClass) {
        /**
         * implementation of your api interface
         */
        mService = RestAPI.getRetrofit().create(serviceClass);
    }
}
