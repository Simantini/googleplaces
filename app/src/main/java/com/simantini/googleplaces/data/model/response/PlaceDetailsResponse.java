package com.simantini.googleplaces.data.model.response;

import java.util.List;

public class PlaceDetailsResponse
{
    private Result result;

    private List<String> html_attributions;

    private String status;

    public Result getResult ()
    {
        return result;
    }

    public void setResult (Result result)
    {
        this.result = result;
    }

    public List<String> getHtml_attributions ()
    {
        return html_attributions;
    }

    public void setHtml_attributions (List<String> html_attributions)
    {
        this.html_attributions = html_attributions;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [result = "+result+", html_attributions = "+html_attributions+", status = "+status+"]";
    }

    public class Result
    {
        private String icon;

        private String place_id;

        private String scope;

        private String adr_address;

        private String url;

        private String reference;

        private Geometry geometry;

        private String utc_offset;

        private String id;

        private List<Photos> photos;

        private String vicinity;

        private List<Address_components> address_components;

        private String name;

        private String formatted_address;

        private List<String> types;

        public String getIcon ()
        {
            return icon;
        }

        public void setIcon (String icon)
        {
            this.icon = icon;
        }

        public String getPlace_id ()
        {
            return place_id;
        }

        public void setPlace_id (String place_id)
        {
            this.place_id = place_id;
        }

        public String getScope ()
        {
            return scope;
        }

        public void setScope (String scope)
        {
            this.scope = scope;
        }

        public String getAdr_address ()
        {
            return adr_address;
        }

        public void setAdr_address (String adr_address)
        {
            this.adr_address = adr_address;
        }

        public String getUrl ()
        {
            return url;
        }

        public void setUrl (String url)
        {
            this.url = url;
        }

        public String getReference ()
        {
            return reference;
        }

        public void setReference (String reference)
        {
            this.reference = reference;
        }

        public Geometry getGeometry ()
        {
            return geometry;
        }

        public void setGeometry (Geometry geometry)
        {
            this.geometry = geometry;
        }

        public String getUtc_offset ()
        {
            return utc_offset;
        }

        public void setUtc_offset (String utc_offset)
        {
            this.utc_offset = utc_offset;
        }

        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public List<Photos> getPhotos ()
        {
            return photos;
        }

        public void setPhotos (List<Photos> photos)
        {
            this.photos = photos;
        }

        public String getVicinity ()
        {
            return vicinity;
        }

        public void setVicinity (String vicinity)
        {
            this.vicinity = vicinity;
        }

        public List<Address_components >getAddress_components ()
        {
            return address_components;
        }

        public void setAddress_components (List<Address_components> address_components)
        {
            this.address_components = address_components;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getFormatted_address ()
        {
            return formatted_address;
        }

        public void setFormatted_address (String formatted_address)
        {
            this.formatted_address = formatted_address;
        }

        public List<String> getTypes ()
        {
            return types;
        }

        public void setTypes (List<String> types)
        {
            this.types = types;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [icon = "+icon+", place_id = "+place_id+", scope = "+scope+", adr_address = "+adr_address+", url = "+url+", reference = "+reference+", geometry = "+geometry+", utc_offset = "+utc_offset+", id = "+id+", photos = "+photos+", vicinity = "+vicinity+", address_components = "+address_components+", name = "+name+", formatted_address = "+formatted_address+", types = "+types+"]";
        }
    }

    public class Photos
    {
        private String photo_reference;

        private String height;

        private List<String> html_attributions;

        private String width;

        public String getPhoto_reference ()
        {
            return photo_reference;
        }

        public void setPhoto_reference (String photo_reference)
        {
            this.photo_reference = photo_reference;
        }

        public String getHeight ()
        {
            return height;
        }

        public void setHeight (String height)
        {
            this.height = height;
        }

        public List<String> getHtml_attributions ()
        {
            return html_attributions;
        }

        public void setHtml_attributions (List<String> html_attributions)
        {
            this.html_attributions = html_attributions;
        }

        public String getWidth ()
        {
            return width;
        }

        public void setWidth (String width)
        {
            this.width = width;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [photo_reference = "+photo_reference+", height = "+height+", html_attributions = "+html_attributions+", width = "+width+"]";
        }
    }

    public class Geometry
    {
        private Viewport viewport;

        private Location location;

        public Viewport getViewport ()
        {
            return viewport;
        }

        public void setViewport (Viewport viewport)
        {
            this.viewport = viewport;
        }

        public Location getLocation ()
        {
            return location;
        }

        public void setLocation (Location location)
        {
            this.location = location;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [viewport = "+viewport+", location = "+location+"]";
        }
    }

    public class Viewport
    {
        private Southwest southwest;

        private Northeast northeast;

        public Southwest getSouthwest ()
        {
            return southwest;
        }

        public void setSouthwest (Southwest southwest)
        {
            this.southwest = southwest;
        }

        public Northeast getNortheast ()
        {
            return northeast;
        }

        public void setNortheast (Northeast northeast)
        {
            this.northeast = northeast;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [southwest = "+southwest+", northeast = "+northeast+"]";
        }
    }
    public class Southwest
    {
        private String lng;

        private String lat;

        public String getLng ()
        {
            return lng;
        }

        public void setLng (String lng)
        {
            this.lng = lng;
        }

        public String getLat ()
        {
            return lat;
        }

        public void setLat (String lat)
        {
            this.lat = lat;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lng = "+lng+", lat = "+lat+"]";
        }
    }


    public class Northeast
    {
        private String lng;

        private String lat;

        public String getLng ()
        {
            return lng;
        }

        public void setLng (String lng)
        {
            this.lng = lng;
        }

        public String getLat ()
        {
            return lat;
        }

        public void setLat (String lat)
        {
            this.lat = lat;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lng = "+lng+", lat = "+lat+"]";
        }
    }

    public class Location
    {
        private String lng;

        private String lat;

        public String getLng ()
        {
            return lng;
        }

        public void setLng (String lng)
        {
            this.lng = lng;
        }

        public String getLat ()
        {
            return lat;
        }

        public void setLat (String lat)
        {
            this.lat = lat;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [lng = "+lng+", lat = "+lat+"]";
        }
    }


    public class Address_components
    {
        private String long_name;

        private List<String> types;

        private String short_name;

        public String getLong_name ()
        {
            return long_name;
        }

        public void setLong_name (String long_name)
        {
            this.long_name = long_name;
        }

        public List<String> getTypes ()
        {
            return types;
        }

        public void setTypes (List<String> types)
        {
            this.types = types;
        }

        public String getShort_name ()
        {
            return short_name;
        }

        public void setShort_name (String short_name)
        {
            this.short_name = short_name;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [long_name = "+long_name+", types = "+types+", short_name = "+short_name+"]";
        }
    }



}