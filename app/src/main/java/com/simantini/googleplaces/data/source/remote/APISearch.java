package com.simantini.googleplaces.data.source.remote;

import com.simantini.googleplaces.constants.APIConstants;
import com.simantini.googleplaces.data.model.response.SearchResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * api to fetch search results for a specific autocomplete input
 * Created by simantini.ranade on 24-06-2017.
 */
public interface APISearch {

    @POST(APIConstants.REST_API_SEARCH)
    Call<SearchResponse> search(@Query("key") String API_KEY,
                                @Query("input") String encode);
}
