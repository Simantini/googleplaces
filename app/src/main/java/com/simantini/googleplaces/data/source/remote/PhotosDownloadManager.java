package com.simantini.googleplaces.data.source.remote;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by simantini.ranade on 25-06-2017.
 */
public class PhotosDownloadManager {

    public static Bitmap savePhotoToDevice(Bitmap bitmap) {

        try {

            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            String filename = System.currentTimeMillis() + ".png";
            File file = new File(extStorageDirectory, filename);
            if (file.exists()) {
                file.delete();
                file = new File(extStorageDirectory, filename);
            }
            OutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isSdCardPresent() {
        return android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);

    }


}