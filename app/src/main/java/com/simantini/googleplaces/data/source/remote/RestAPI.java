package com.simantini.googleplaces.data.source.remote;


import com.simantini.googleplaces.constants.APIConstants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class used for network communication using Retrofit library
 * Created by simantini.ranade on 24-06-2017.
 */
public class RestAPI {
    private static Environment environment = Environment.DEV;

    private static Retrofit retrofit;

    static Retrofit getRetrofit() {
        if (retrofit == null) {
            String baseUrl = environment == Environment.PROD ? APIConstants.REST_BASE_URI_PROD :
                    APIConstants.REST_BASE_URI_DEV;

            OkHttpClient client = new OkHttpClient.Builder()
                    .readTimeout(1, TimeUnit.MINUTES)
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    private enum Environment {
        DEV, PROD
    }
}
